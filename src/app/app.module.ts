import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http'; 

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CompanyHistory } from '../pages/company-history/company-history';
import { CapsulesList } from '../pages/capsules-list/capsules-list';
import { CapsuleDetails } from "../pages/capsule-details/capsule-details";
import { LaunchesList } from "../pages/launches-list/launches-list";
import { LaunchDetails } from '../pages/launch-details/launch-details';
import { CoresList } from "../pages/cores-list/cores-list";
import { CoreDetails } from '../pages/core-details/core-details';
import { LaunchpadsList } from "../pages/launchpads-list/launchpads-list";
import { LaunchpadDetails } from '../pages/launchpad-details/launchpad-details';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SpiceProvider } from '../providers/spice/spice';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CompanyHistory,
    CapsulesList,
    CapsuleDetails,
    LaunchesList,
    LaunchDetails,
    CoresList,
    CoreDetails,
    LaunchpadsList,
    LaunchpadDetails,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CompanyHistory,
    CapsulesList,
    CapsuleDetails,
    LaunchesList,
    LaunchDetails,
    CoresList,
    CoreDetails,
    LaunchpadsList,
    LaunchpadDetails,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SpiceProvider
  ]
})
export class AppModule {}
