import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LaunchpadsList } from "../pages/launchpads-list/launchpads-list";
import { CapsulesList } from '../pages/capsules-list/capsules-list';
import { LaunchesList } from "../pages/launches-list/launches-list";
import { CoresList } from "../pages/cores-list/cores-list";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    this.pages = [
      { title: 'Spice X Information', component: HomePage },
      { title: 'List of launchpads', component: LaunchpadsList },
      { title: 'List of launches', component: LaunchesList },
      { title: 'List of capsules', component: CapsulesList },
      { title: 'List of cores', component: CoresList },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
  
}
