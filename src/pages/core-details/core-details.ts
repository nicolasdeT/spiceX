import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
	selector: 'core-details',
	templateUrl: 'core-details.html'
})

export class CoreDetails {
	
	core: any;

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.core = navParams.get('core');
	}
}
