import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SpiceProvider } from "../../providers/spice/spice";
import { LaunchDetails } from '../launch-details/launch-details';

@Component({
	selector: 'launches-list',
	templateUrl: 'launches-list.html',
})
export class LaunchesList {
	order = "desc";
	launches = new Array<any>();
	filter = new Array<any>();

	constructor(public navCtrl: NavController, private spiceProvider: SpiceProvider, public navParams: NavParams) {
		this.updateData();
	}

	getLaunchDetails(launch) {
		this.navCtrl.push(LaunchDetails, {
			launch: launch
		});
	}

	changeOrder() {
		this.order == "desc" ? this.order = "asc" : this.order = "desc";
		this.updateData();
	}

	resetFilter() {
		this.filter = this.launches;
	}

	getFilter(event: any) {
		this.resetFilter();
		const val = event.target.value;
		if (val && val.trim() != '') {
			this.filter = this.filter.filter((item) => {
				return (item.mission_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}

	updateData() {
		this.spiceProvider.getLaunches({ order: this.order }).subscribe(data => {
			this.launches = data;
			this.filter = data;
		})
	}

}
