import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SpiceProvider } from "../../providers/spice/spice";

@Component({
  selector: 'company-history',
  templateUrl: 'company-history.html'
})

export class CompanyHistory {
  order = "desc";
  companyHistory = new Array<any>(); 
	filter = new Array<any>();
  
  constructor(public navCtrl: NavController, private spiceProvider: SpiceProvider) {
    this.updateData();
  }

  updateData() {
    this.spiceProvider.getCompanyHistory({ order: this.order }).subscribe( data => {
      this.filter = data;
      this.companyHistory = data; 
    })
  }
  
  changeOrder() {
		this.order == "desc" ? this.order = "asc" : this.order = "desc";
		this.updateData();
	}


  resetFilter() {
		this.filter = this.companyHistory;
	}

	getFilter(event: any) {
		this.resetFilter();
		const val = event.target.value;
		if (val && val.trim() != '') {
			this.filter = this.filter.filter((item) => {
				return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}
    
}
