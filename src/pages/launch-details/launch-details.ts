import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SpiceProvider } from "../../providers/spice/spice";
import { LaunchpadDetails } from '../launchpad-details/launchpad-details';

@Component({
	selector: 'launch-details',
	templateUrl: 'launch-details.html',
})

export class LaunchDetails {

	launch: any;

	constructor(public navCtrl: NavController, private spiceProvider: SpiceProvider, public navParams: NavParams) {
		this.launch = navParams.get('launch');
	}

	getLaunchpadDetails(launchpad) {
		this.spiceProvider.getLaunchpad({ launchpad_id: launchpad }).subscribe(data => {
			this.navCtrl.push(LaunchpadDetails, {
				launchpad: data
			});
		})
	}
}
